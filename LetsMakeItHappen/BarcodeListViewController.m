//
//  BarcodeListViewController.m
//  LetsMakeItHappen
//
//  Created by Raul Catena on 10/12/14.
//  Copyright (c) 2014 CatApps. All rights reserved.
//

#import "BarcodeListViewController.h"

@interface BarcodeListViewController ()

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableDictionary *dict;

@end

@implementation BarcodeListViewController

-(id)initWithNames:(NSArray *)array{
    self = [super init];
    if (self) {
        self.names = array.mutableCopy;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView = [[UITableView alloc]initWithFrame:self.view.frame];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    _tableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _names.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"AnId";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    }
    
    for (UIView *view in cell.contentView.subviews) {
        if (view.tag == 1) {
            [view removeFromSuperview];
        }
    }
    
    if ([_dict valueForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]) {
        UIImageView *imageV = [[UIImageView alloc]initWithImage:[_dict objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
        imageV.tag = 1;
        imageV.frame = CGRectMake(10, 10, cell.bounds.size.width - 20, cell.bounds.size.width/4);
        [cell.contentView addSubview:imageV];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, cell.bounds.size.width / 4 + 10, cell.bounds.size.width - 20, 20)];
        label.tag = 1;
        label.text = [_names objectAtIndex:indexPath.row];
        label.textColor = [UIColor darkGrayColor];
        [cell.contentView addSubview:label];
    }else{
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, cell.bounds.size.height / 2 - 10, cell.bounds.size.width - 20, 20)];
        label.tag = 1;
        label.text = [_names objectAtIndex:indexPath.row];
        label.textColor = [UIColor darkGrayColor];
        [cell.contentView addSubview:label];
        
        NSString *barCodeValue = [NSString stringWithFormat:@"%i", [[_names objectAtIndex:indexPath.row]intValue]];
        NSLog(@"Will pass %@", barCodeValue);
        
        NSString *barCodeURL = [NSString stringWithFormat:@"http://www.barcodesinc.com/generator/image.php?code=%i&style=197&type=C128B&width=200&height=100&xres=1&font=3", rand() % 20000];
        dispatch_queue_t aQueue = dispatch_queue_create("aQue", NULL);
        dispatch_async(aQueue, ^{
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:barCodeURL]];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!_dict) {
                    self.dict = [NSMutableDictionary dictionaryWithCapacity:_names.count];
                }
                if (![_dict valueForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]) {
                    [_dict setObject:[UIImage imageWithData:data] forKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]];
                }
                [self.tableView reloadData];
            });
        });
    }
    
    return cell;
}

static inline double radians (double degrees) {return degrees * M_PI/180;}

-(UIImage *)rotate:(UIImage *)src withOrientation:(UIImageOrientation)orientation
{
    UIGraphicsBeginImageContext(src.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, radians(90));
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, radians(-90));
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, radians(90));
    }
    
    [src drawAtPoint:CGPointMake(0, 0)];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([_dict valueForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]) {
        UIImage *image = [_dict objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]];
        UIImage * rotate = [[UIImage alloc] initWithCGImage: image.CGImage
                                                             scale: 1.0
                                                       orientation: UIImageOrientationRight];
        UIImageView *imageV = [[UIImageView alloc]initWithImage:rotate];
        UIViewController *viewCont = [[UIViewController alloc]init];
        imageV.frame = CGRectMake(10, 10, self.view.bounds.size.width - 20, self.view.bounds.size.height - 20);
        //[viewCont.view addSubview:imageV];
        
        UINavigationController *navCon = [[UINavigationController alloc]initWithRootViewController:viewCont];
        viewCont.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(close)];
        
        [self presentViewController:navCon animated:YES completion:^{
            [viewCont.view addSubview:imageV];
        }];

    }
}



-(void)close{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return self.view.bounds.size.width/4 + 30;
}

@end
