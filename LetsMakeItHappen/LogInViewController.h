//
//  LogInViewController.h
//  LetsMakeItHappen
//
//  Created by Raul Catena on 10/12/14.
//  Copyright (c) 2014 CatApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogInViewController : UIViewController <UITextFieldDelegate>

-(IBAction)enter:(id)sender;

@end
