//
//  LogInViewController.m
//  LetsMakeItHappen
//
//  Created by Raul Catena on 10/12/14.
//  Copyright (c) 2014 CatApps. All rights reserved.
//

#import "LogInViewController.h"
#import "DashBoardViewController.h"

@interface LogInViewController ()

@property (nonatomic, weak) UITextField *field;

@end

@implementation LogInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"PullTogether - LogIn";
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resign)]];
}

-(void)resign{
    [_field resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    _field = textField;
}

-(void)enter:(id)sender{
    DashBoardViewController *dash = [[DashBoardViewController alloc]init];
    [self.navigationController setViewControllers:[NSArray arrayWithObject:dash]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
