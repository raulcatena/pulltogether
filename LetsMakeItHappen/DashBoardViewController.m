//
//  DashBoardViewController.m
//  LetsMakeItHappen
//
//  Created by Raul Catena on 10/12/14.
//  Copyright (c) 2014 CatApps. All rights reserved.
//

#import "DashBoardViewController.h"
#import "PlanViewController.h"
#import "LogInViewController.h"

@interface DashBoardViewController ()

@end

@implementation DashBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITableView *table = [[UITableView alloc]initWithFrame:self.view.frame];
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
    // Do any additional setup after loading the view.
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Log out" style:UIBarButtonItemStyleDone target:self action:@selector(logout)];
}

-(void)logout{
    [self.navigationController setViewControllers:[NSArray arrayWithObject:[[LogInViewController alloc]init]] animated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 2;
    }
    return 4;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"My plans";
    }
    return @"Invited";
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"AnId";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"Beach Volley";
            }else{
                cell.textLabel.text = @"Home moving";
            }
        }
            break;
        
        case 1:
        {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"Hike Pyrenees";
            }else if(indexPath.row == 1){
                cell.textLabel.text = @"Hackathon organization";
            }else if(indexPath.row == 2){
                cell.textLabel.text = @"Salsa night";
            }else if(indexPath.row == 3){
                cell.textLabel.text = @"Entrepreneur's club";
            }else{
                cell.textLabel.text = @"Other plan";
            }
        }
            break;
            
        default:
            break;
    }
    cell.textLabel.textColor = [UIColor darkGrayColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PlanViewController *plan = [[PlanViewController alloc]init];
    if (indexPath.section == 0) {
        plan.isOrganizer = YES;
    }else{
        plan.isOrganizer = NO;
    }
    plan.title = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
    [self.navigationController pushViewController:plan animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
