//
//  PlanViewController.m
//  LetsMakeItHappen
//
//  Created by Raul Catena on 10/12/14.
//  Copyright (c) 2014 CatApps. All rights reserved.
//

#import "PlanViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "BarcodeListViewController.h"

@interface PlanViewController ()

@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) NSMutableArray *taken;
@property (nonatomic, strong) NSMutableArray *faces;
@property (nonatomic, strong) NSMutableArray *namesItems;

@property (nonatomic, strong) NSMutableArray *myThings;

@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureDevice *device;
@property (nonatomic, strong) AVCaptureDeviceInput *input;
@property (nonatomic, strong) AVCaptureMetadataOutput *output;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *prevLayer;
@property (nonatomic, strong) UITableView *tableView;




@end

@implementation PlanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.images = @[
                    [UIImage imageNamed:@"server.jpg"],
                    [UIImage imageNamed:@"server2.jpg"],
                    [UIImage imageNamed:@"server3.jpg"],
                    [UIImage imageNamed:@"server4.jpg"],
                    [UIImage imageNamed:@"server5.jpg"],
                    [UIImage imageNamed:@"server6.jpg"],
                    [UIImage imageNamed:@"server7.jpg"],
                    [UIImage imageNamed:@"server8.jpg"],
                    [UIImage imageNamed:@"server9.jpg"],
                    [UIImage imageNamed:@"server10.jpg"],
                    ].mutableCopy;
    
    self.namesItems = @[
                    @"Water",
                    @"Utensils",
                    @"Mustard",
                    @"Bananas",
                    @"Chips",
                    @"Soap",
                    @"Ice Cream",
                    @"Soup",
                    @"Coffee",
                    @"Syrup",
                    ].mutableCopy;
    
    self.taken = @[
                   [NSNumber numberWithInt:0],
                   [NSNumber numberWithInt:1],
                   [NSNumber numberWithInt:0],
                   [NSNumber numberWithInt:2],
                   [NSNumber numberWithInt:2],
                   [NSNumber numberWithInt:0],
                   [NSNumber numberWithInt:3],
                   [NSNumber numberWithInt:4],
                   [NSNumber numberWithInt:5],
                   [NSNumber numberWithInt:0],
                   ].mutableCopy;
    
    self.faces = @[
                   [UIImage imageNamed:@"1.jpg"],
                   [UIImage imageNamed:@"2.jpg"],
                   [UIImage imageNamed:@"3.jpg"],
                   [UIImage imageNamed:@"4.jpg"],
                   [UIImage imageNamed:@"5.jpg"],
                   [UIImage imageNamed:@"6.jpg"],
                   [UIImage imageNamed:@"7.jpg"],
                   ].mutableCopy;
    
    self.myThings = [NSMutableArray arrayWithCapacity:self.namesItems.count];
    
    _tableView = [[UITableView alloc]initWithFrame:self.view.frame];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    _tableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_isOrganizer) {
        //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(barCode)];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"My Barcodes" style:UIBarButtonItemStyleDone target:self action:@selector(showBarcodes)];
    }
}

-(void)showBarcodes{
    /*NSString *barCodeValue = @"0123456789";
    NSString *barCodeURL = [NSString stringWithFormat:@"http://www.barcodesinc.com/generator/image.php?code=%@&style=197&type=C128B&width=200&height=100&xres=1&font=3", barCodeValue];
    barCodeImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:barCodeURL]]];*/
    
    BarcodeListViewController *barcodeList = [[BarcodeListViewController alloc]initWithNames:[NSArray arrayWithArray:_myThings]];
    [self.navigationController pushViewController:barcodeList animated:YES];
}


-(void)barCode{
    _session = [[AVCaptureSession alloc] init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (_input) {
        [_session addInput:_input];
    } else {
        NSLog(@"Error: %@", error);
    }
    
    _output = [[AVCaptureMetadataOutput alloc] init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];
    
    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
    
    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _prevLayer.frame = self.view.bounds;
    _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:_prevLayer];
    
    [_session startRunning];
     
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    NSLog(@"Am I here");
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    NSString *detectionString = nil;
    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    for (AVMetadataObject *metadata in metadataObjects) {
        for (NSString *type in barCodeTypes) {
            if ([metadata.type isEqualToString:type])
            {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                highlightViewRect = barCodeObject.bounds;
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                break;
            }
        }
        
        if (detectionString != nil)
        {
            NSLog(@"Detection string is %@", detectionString);
            [_namesItems addObject:detectionString];
            [_taken addObject:[NSNumber numberWithInt:0]];
            break;
        }
        
    }
    [_prevLayer removeFromSuperlayer];
    [_tableView reloadData];
    //_highlightView.frame = highlightViewRect;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _namesItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"AnId";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    }
    
    for (UIView *view in cell.contentView.subviews) {
        if ([view isMemberOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    
    cell.textLabel.text = [_namesItems objectAtIndex:indexPath.row];
    //if (_images.count> indexPath.row) {
        cell.imageView.image = [_images objectAtIndex:indexPath.row];
    //}else{
      //  cell.imageView.image = nil;
//    }
    
    
    if ([[_taken objectAtIndex:indexPath.row]intValue] != 0) {
        UIImageView *iView = [[UIImageView alloc]initWithFrame:CGRectMake(cell.bounds.size.width - 60, 2.5f, 50, 50)];
        iView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.jpg", [[_taken objectAtIndex:indexPath.row]intValue]]];
        iView.layer.cornerRadius = 25;
        iView.clipsToBounds = YES;
        iView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [cell.contentView addSubview:iView];
        
        cell.textLabel.textColor = [UIColor lightGrayColor];
    }else{
        cell.textLabel.textColor = [UIColor darkGrayColor];
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if ([[_taken objectAtIndex:indexPath.row]intValue] != 0) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }else{
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if ([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark) {
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
            if ([_myThings containsObject:[_namesItems objectAtIndex:indexPath.row]]) {
                [_myThings removeObject:[_namesItems objectAtIndex:indexPath.row]];
            }
        }else{
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
            if (![_myThings containsObject:[_namesItems objectAtIndex:indexPath.row]]) {
                [_myThings addObject:[_namesItems objectAtIndex:indexPath.row]];
            }
        }
    }
}

@end
