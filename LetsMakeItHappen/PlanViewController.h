//
//  PlanViewController.h
//  LetsMakeItHappen
//
//  Created by Raul Catena on 10/12/14.
//  Copyright (c) 2014 CatApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PlanViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic, assign) BOOL isOrganizer;

@end
