//
//  BarcodeListViewController.h
//  LetsMakeItHappen
//
//  Created by Raul Catena on 10/12/14.
//  Copyright (c) 2014 CatApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BarcodeListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *names;

-(id)initWithNames:(NSArray *)array;

@end
