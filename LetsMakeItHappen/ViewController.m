//
//  ViewController.m
//  LetsMakeItHappen
//
//  Created by Raul Catena on 10/11/14.
//  Copyright (c) 2014 CatApps. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

-(void)adjustIOS7{
    if ([[[[[UIDevice currentDevice] systemVersion]
           componentsSeparatedByString:@"."] objectAtIndex:0] intValue] >= 7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self adjustIOS7];
    [self.view setBackgroundColor:[UIColor orangeColor]];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.raulcatena.com/letsmakeithappen"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
