-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2014 at 02:37 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hackzurich`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
`id` int(11) NOT NULL,
  `country` varchar(255) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin2 AUTO_INCREMENT=221 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country`) VALUES
(1, 'United States'),
(2, 'United Kingdom'),
(3, 'Norway'),
(4, 'Greece'),
(5, 'Afghanistan'),
(6, 'Albania'),
(7, 'Algeria'),
(8, 'American Samoa'),
(9, 'Andorra'),
(10, 'Angola'),
(11, 'Anguilla'),
(12, 'Antigua & Barbuda'),
(13, 'Antilles, Netherlands'),
(182, 'Senegal'),
(15, 'Argentina'),
(16, 'Armenia'),
(17, 'Aruba'),
(18, 'Australia'),
(19, 'Austria'),
(20, 'Azerbaijan'),
(21, 'Bahamas, The'),
(22, 'Bahrain'),
(23, 'Bangladesh'),
(24, 'Barbados'),
(25, 'Belarus'),
(26, 'Belgium'),
(27, 'Belize'),
(28, 'Benin'),
(29, 'Bermuda'),
(30, 'Bhutan'),
(31, 'Bolivia'),
(32, 'Bosnia and Herzegovina'),
(33, 'Botswana'),
(34, 'Brazil'),
(35, 'British Virgin Islands'),
(36, 'Brunei Darussalam'),
(37, 'Bulgaria'),
(38, 'Burkina Faso'),
(39, 'Burundi'),
(40, 'Cambodia'),
(41, 'Cameroon'),
(42, 'Canada'),
(43, 'Cape Verde'),
(44, 'Cayman Islands'),
(45, 'Central African Republic'),
(46, 'Chad'),
(47, 'Chile'),
(48, 'China'),
(49, 'Colombia'),
(50, 'Comoros'),
(51, 'Congo'),
(52, 'Congo'),
(53, 'Cook Islands'),
(54, 'Costa Rica'),
(55, 'Cote D''Ivoire'),
(56, 'Croatia'),
(57, 'Cuba'),
(58, 'Cyprus'),
(59, 'Czech Republic'),
(60, 'Denmark'),
(61, 'Djibouti'),
(62, 'Dominica'),
(63, 'Dominican Republic'),
(64, 'East Timor (Timor-Leste)'),
(65, 'Ecuador'),
(66, 'Egypt'),
(67, 'El Salvador'),
(68, 'Equatorial Guinea'),
(69, 'Eritrea'),
(70, 'Estonia'),
(71, 'Ethiopia'),
(72, 'Falkland Islands'),
(73, 'Faroe Islands'),
(74, 'Fiji'),
(75, 'Finland'),
(76, 'France'),
(77, 'French Guiana'),
(78, 'French Polynesia'),
(79, 'Gabon'),
(80, 'Gambia, the'),
(81, 'Georgia'),
(82, 'Germany'),
(83, 'Ghana'),
(84, 'Gibraltar'),
(86, 'Greenland'),
(87, 'Grenada'),
(88, 'Guadeloupe'),
(89, 'Guam'),
(90, 'Guatemala'),
(91, 'Guernsey and Alderney'),
(92, 'Guinea'),
(93, 'Guinea-Bissau'),
(94, 'Guinea, Equatorial'),
(95, 'Guiana, French'),
(96, 'Guyana'),
(97, 'Haiti'),
(179, 'San Marino'),
(99, 'Honduras'),
(100, 'Hong Kong, (China)'),
(101, 'Hungary'),
(102, 'Iceland'),
(103, 'India'),
(104, 'Indonesia'),
(105, 'Iran, Islamic Republic of'),
(106, 'Iraq'),
(107, 'Ireland'),
(108, 'Israel'),
(109, 'Ivory Coast (Cote d''Ivoire)'),
(110, 'Italy'),
(111, 'Jamaica'),
(112, 'Japan'),
(113, 'Jersey'),
(114, 'Jordan'),
(115, 'Kazakhstan'),
(116, 'Kenya'),
(117, 'Kiribati'),
(118, 'Korea, (South) Rep. of'),
(119, 'Kuwait'),
(120, 'Kyrgyzstan'),
(121, 'Lao People''s Dem. Rep.'),
(122, 'Latvia'),
(123, 'Lebanon'),
(124, 'Lesotho'),
(125, 'Libyan Arab Jamahiriya'),
(126, 'Liechtenstein'),
(127, 'Lithuania'),
(128, 'Luxembourg'),
(129, 'Macao, (China)'),
(130, 'Macedonia, TFYR'),
(131, 'Madagascar'),
(132, 'Malawi'),
(133, 'Malaysia'),
(134, 'Maldives'),
(135, 'Mali'),
(136, 'Malta'),
(137, 'Martinique'),
(138, 'Mauritania'),
(139, 'Mauritius'),
(140, 'Mexico'),
(141, 'Micronesia'),
(142, 'Moldova, Republic of'),
(143, 'Monaco'),
(144, 'Mongolia'),
(145, 'Montenegro'),
(146, 'Morocco'),
(147, 'Mozambique'),
(148, 'Myanmar (ex-Burma)'),
(149, 'Namibia'),
(150, 'Nepal'),
(151, 'Netherlands'),
(152, 'New Caledonia'),
(153, 'New Zealand'),
(154, 'Nicaragua'),
(155, 'Niger'),
(156, 'Nigeria'),
(157, 'Northern Mariana Islands'),
(159, 'Oman'),
(160, 'Pakistan'),
(161, 'Palestinian Territory'),
(162, 'Panama'),
(163, 'Papua New Guinea'),
(164, 'Paraguay'),
(165, 'Peru'),
(166, 'Philippines'),
(167, 'Poland'),
(168, 'Portugal'),
(170, 'Qatar'),
(171, 'Reunion'),
(172, 'Romania'),
(173, 'Russian Federation'),
(174, 'Rwanda'),
(175, 'Saint Kitts and Nevis'),
(176, 'Saint Lucia'),
(177, 'St. Vincent & the Grenad.'),
(178, 'Samoa'),
(180, 'Sao Tome and Principe'),
(181, 'Saudi Arabia'),
(183, 'Serbia'),
(184, 'Seychelles'),
(185, 'Singapore'),
(186, 'Slovakia'),
(187, 'Slovenia'),
(188, 'Solomon Islands'),
(189, 'Somalia'),
(190, 'South Sudan'),
(191, 'Spain'),
(192, 'Sri Lanka'),
(193, 'Sudan'),
(194, 'Suriname'),
(195, 'Swaziland'),
(196, 'Sweden'),
(197, 'Switzerland'),
(198, 'Syrian Arab Republic'),
(199, 'Taiwan'),
(200, 'Tajikistan'),
(201, 'Tanzania, United Rep. of'),
(202, 'Thailand'),
(203, 'Togo'),
(204, 'Tonga'),
(205, 'Trinidad & Tobago'),
(206, 'Tunisia'),
(207, 'Turkey'),
(208, 'Turkmenistan'),
(209, 'Uganda'),
(210, 'Ukraine'),
(211, 'United Arab Emirates'),
(212, 'Uruguay'),
(213, 'Uzbekistan'),
(214, 'Vanuatu'),
(215, 'Venezuela'),
(216, 'Viet Nam'),
(217, 'Virgin Islands, U.S.'),
(218, 'Yemen'),
(219, 'Zambia'),
(220, 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `dialing_code`
--

CREATE TABLE IF NOT EXISTS `dialing_code` (
`id` int(11) NOT NULL,
  `iso2` char(2) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dialing_code` int(4) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=247 ;

--
-- Dumping data for table `dialing_code`
--

INSERT INTO `dialing_code` (`id`, `iso2`, `name`, `dialing_code`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AX', 'Aland Islands', 0),
(3, 'AL', 'Albania', 355),
(4, 'DZ', 'Algeria', 213),
(5, 'AS', 'American Samoa', 1),
(6, 'AD', 'Andorra', 376),
(7, 'AO', 'Angola', 244),
(8, 'AI', 'Anguilla', 1),
(9, 'AQ', 'Antarctica', 0),
(10, 'AG', 'Antigua and Barbuda', 1),
(11, 'AR', 'Argentina', 54),
(12, 'AM', 'Armenia', 374),
(13, 'AW', 'Aruba', 297),
(14, 'AU', 'Australia', 61),
(15, 'AT', 'Austria', 43),
(16, 'AZ', 'Azerbaijan', 994),
(17, 'BS', 'Bahamas', 1),
(18, 'BH', 'Bahrain', 973),
(19, 'BD', 'Bangladesh', 880),
(20, 'BB', 'Barbados', 1),
(21, 'BY', 'Belarus', 375),
(22, 'BE', 'Belgium', 32),
(23, 'BZ', 'Belize', 501),
(24, 'BJ', 'Benin', 229),
(25, 'BM', 'Bermuda', 1),
(26, 'BT', 'Bhutan', 975),
(27, 'BO', 'Bolivia', 591),
(28, 'BA', 'Bosnia and Herzegovina', 387),
(29, 'BW', 'Botswana', 267),
(30, 'BV', 'Bouvet Island', 0),
(31, 'BR', 'Brazil', 55),
(32, 'IO', 'British Indian Ocean Territory', 1),
(33, 'BN', 'Brunei Darussalam', 673),
(34, 'BG', 'Bulgaria', 359),
(35, 'BF', 'Burkina Faso', 226),
(36, 'BI', 'Burundi', 257),
(37, 'KH', 'Cambodia', 855),
(38, 'CM', 'Cameroon', 237),
(39, 'CA', 'Canada', 1),
(40, 'CV', 'Cape Verde', 238),
(41, 'KY', 'Cayman Islands', 1),
(42, 'CF', 'Central African Republic', 236),
(43, 'TD', 'Chad', 235),
(44, 'CL', 'Chile', 56),
(45, 'CN', 'China', 86),
(46, 'CX', 'Christmas Island', 0),
(47, 'CC', 'Cocos (Keeling) Islands', 0),
(48, 'CO', 'Colombia', 57),
(49, 'KM', 'Comoros', 269),
(50, 'CG', 'Congo', 242),
(51, 'CD', 'Congo, Democratic Republic of the', 243),
(52, 'CK', 'Cook Islands', 682),
(53, 'CR', 'Costa Rica', 506),
(54, 'CI', 'Côte d''Ivoire', 225),
(55, 'HR', 'Croatia', 385),
(56, 'CU', 'Cuba', 53),
(57, 'CY', 'Cyprus', 357),
(58, 'CZ', 'Czech Republic', 420),
(59, 'DK', 'Denmark', 45),
(60, 'DJ', 'Djibouti', 253),
(61, 'DM', 'Dominica', 1),
(62, 'DO', 'Dominican Republic', 1),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'FK', 'Falkland Islands (Malvinas)', 500),
(71, 'FO', 'Faroe Islands', 298),
(72, 'FJ', 'Fiji', 679),
(73, 'FI', 'Finland', 358),
(74, 'FR', 'France', 33),
(75, 'GF', 'French Guiana', 594),
(76, 'PF', 'French Polynesia', 689),
(77, 'TF', 'French Southern Territories', 262),
(78, 'GA', 'Gabon', 241),
(79, 'GM', 'Gambia', 220),
(80, 'GE', 'Georgia', 995),
(81, 'DE', 'Germany', 49),
(82, 'GH', 'Ghana', 233),
(83, 'GI', 'Gibraltar', 350),
(84, 'GR', 'Greece', 30),
(85, 'GL', 'Greenland', 299),
(86, 'GD', 'Grenada', 1),
(87, 'GP', 'Guadeloupe', 590),
(88, 'GU', 'Guam', 1),
(89, 'GT', 'Guatemala', 502),
(90, 'GG', 'Guernsey', 0),
(91, 'GN', 'Guinea', 224),
(92, 'GW', 'Guinea-Bissau', 245),
(93, 'GY', 'Guyana', 592),
(94, 'HT', 'Haiti', 509),
(95, 'HM', 'Heard Island and McDonald Islands', 0),
(96, 'VA', 'Holy See (Vatican City State)', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran, Islamic Republic of', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IM', 'Isle of Man', 0),
(107, 'IL', 'Israel', 972),
(108, 'IT', 'Italy', 39),
(109, 'JM', 'Jamaica', 1),
(110, 'JP', 'Japan', 81),
(111, 'JE', 'Jersey', 0),
(112, 'JO', 'Jordan', 962),
(113, 'KZ', 'Kazakhstan', 7),
(114, 'KE', 'Kenya', 254),
(115, 'KI', 'Kiribati', 686),
(116, 'KP', 'Korea, Democratic People''s Republic of', 850),
(117, 'KR', 'Korea, Republic of', 82),
(118, 'KW', 'Kuwait', 965),
(119, 'KG', 'Kyrgyzstan', 996),
(120, 'LA', 'Lao People''s Democratic Republic', 856),
(121, 'LV', 'Latvia', 371),
(122, 'LB', 'Lebanon', 961),
(123, 'LS', 'Lesotho', 266),
(124, 'LR', 'Liberia', 231),
(125, 'LY', 'Libyan Arab Jamahiriya', 218),
(126, 'LI', 'Liechtenstein', 423),
(127, 'LT', 'Lithuania', 370),
(128, 'LU', 'Luxembourg', 352),
(129, 'MO', 'Macao', 853),
(130, 'MK', 'Macedonia, the former Yugoslav Republic of', 0),
(131, 'MG', 'Madagascar', 261),
(132, 'MW', 'Malawi', 265),
(133, 'MY', 'Malaysia', 60),
(134, 'MV', 'Maldives', 960),
(135, 'ML', 'Mali', 223),
(136, 'MT', 'Malta', 356),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia, Federated States of', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'ME', 'Montenegro', 382),
(148, 'MS', 'Montserrat', 1),
(149, 'MA', 'Morocco', 212),
(150, 'MZ', 'Mozambique', 258),
(151, 'MM', 'Myanmar', 95),
(152, 'NA', 'Namibia', 264),
(153, 'NR', 'Nauru', 674),
(154, 'NP', 'Nepal', 977),
(155, 'NL', 'Netherlands', 31),
(156, 'AN', 'Netherlands Antilles', 599),
(157, 'NC', 'New Caledonia', 687),
(158, 'NZ', 'New Zealand', 64),
(159, 'NI', 'Nicaragua', 505),
(160, 'NE', 'Niger', 227),
(161, 'NG', 'Nigeria', 234),
(162, 'NU', 'Niue', 683),
(163, 'NF', 'Norfolk Island', 0),
(164, 'MP', 'Northern Mariana Islands', 1),
(165, 'NO', 'Norway', 47),
(166, 'OM', 'Oman', 968),
(167, 'PK', 'Pakistan', 92),
(168, 'PW', 'Palau', 680),
(169, 'PS', 'Palestinian Territory, Occupied', 0),
(170, 'PA', 'Panama', 507),
(171, 'PG', 'Papua New Guinea', 675),
(172, 'PY', 'Paraguay', 595),
(173, 'PE', 'Peru', 51),
(174, 'PH', 'Philippines', 63),
(175, 'PN', 'Pitcairn', 0),
(176, 'PL', 'Poland', 48),
(177, 'PT', 'Portugal', 351),
(178, 'PR', 'Puerto Rico', 1),
(179, 'QA', 'Qatar', 974),
(180, 'RE', 'R閡nion', 0),
(181, 'RO', 'Romania', 40),
(182, 'RU', 'Russian Federation', 7),
(183, 'RW', 'Rwanda', 250),
(184, 'BL', 'Saint Barth閘emy', 0),
(185, 'SH', 'Saint Helena', 290),
(186, 'KN', 'Saint Kitts and Nevis', 1),
(187, 'LC', 'Saint Lucia', 1),
(188, 'MF', 'Saint Martin (French part)', 0),
(189, 'PM', 'Saint Pierre and Miquelon', 508),
(190, 'VC', 'Saint Vincent and the Grenadines', 1),
(191, 'WS', 'Samoa', 685),
(192, 'SM', 'San Marino', 378),
(193, 'ST', 'Sao Tome and Principe', 239),
(194, 'SA', 'Saudi Arabia', 966),
(195, 'SN', 'Senegal', 221),
(196, 'RS', 'Serbia', 381),
(197, 'SC', 'Seychelles', 248),
(198, 'SL', 'Sierra Leone', 232),
(199, 'SG', 'Singapore', 65),
(200, 'SK', 'Slovak', 421),
(201, 'SI', 'Slovenia', 386),
(202, 'SB', 'Solomon Islands', 677),
(203, 'SO', 'Somalia', 252),
(204, 'ZA', 'South Africa', 27),
(205, 'GS', 'South Georgia and the South Sandwich Islands', 0),
(206, 'ES', 'Spain', 34),
(207, 'LK', 'Sri Lanka', 94),
(208, 'SD', 'Sudan', 249),
(209, 'SR', 'Suriname', 597),
(210, 'SJ', 'Svalbard and Jan Mayen', 0),
(211, 'SZ', 'Swaziland', 268),
(212, 'SE', 'Sweden', 46),
(213, 'CH', 'Switzerland', 41),
(214, 'SY', 'Syrian Arab Republic', 963),
(215, 'TW', 'Taiwan, Province of China', 886),
(216, 'TJ', 'Tajikistan', 992),
(217, 'TZ', 'Tanzania, United Republic of', 255),
(218, 'TH', 'Thailand', 66),
(219, 'TL', 'Timor-Leste', 0),
(220, 'TG', 'Togo', 228),
(221, 'TK', 'Tokelau', 690),
(222, 'TO', 'Tonga', 676),
(223, 'TT', 'Trinidad and Tobago', 1),
(224, 'TN', 'Tunisia', 216),
(225, 'TR', 'Turkey', 90),
(226, 'TM', 'Turkmenistan', 993),
(227, 'TC', 'Turks and Caicos Islands', 1),
(228, 'TV', 'Tuvalu', 688),
(229, 'UG', 'Uganda', 256),
(230, 'UA', 'Ukraine', 380),
(231, 'AE', 'United Arab Emirates', 971),
(232, 'GB', 'United Kingdom of Great Britain and Northern Ireland', 44),
(233, 'US', 'United States of America', 1),
(234, 'UM', 'United States Minor Outlying Islands', 0),
(235, 'UY', 'Uruguay', 598),
(236, 'UZ', 'Uzbekistan', 998),
(237, 'VU', 'Vanuatu', 678),
(238, 'VE', 'Venezuela', 58),
(239, 'VN', 'Viet Nam', 84),
(240, 'VG', 'Virgin Islands, British', 0),
(241, 'VI', 'Virgin Islands, U.S.', 0),
(242, 'WF', 'Wallis and Futuna', 681),
(243, 'EH', 'Western Sahara', 0),
(244, 'YE', 'Yemen', 967),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
`id` int(8) NOT NULL,
  `itemname` varchar(255) NOT NULL,
  `quantity` int(8) NOT NULL,
  `planid` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `userid` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `itemname`, `quantity`, `planid`, `unit`, `img`, `userid`) VALUES
(12, 'Meet 3 KG', 0, '6272cab474937b0e39a5e06fdb8c4cee', '', '', '0'),
(13, 'Water 2 Liters', 0, '6272cab474937b0e39a5e06fdb8c4cee', '', '', '0'),
(14, 'Chips 4 boxes', 0, '6272cab474937b0e39a5e06fdb8c4cee', '', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE IF NOT EXISTS `plans` (
`id` int(8) NOT NULL,
  `planid` varchar(255) NOT NULL,
  `plan` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `personid` int(8) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `planid`, `plan`, `date`, `personid`) VALUES
(66, '5daa21de60354e5b046b52a3398e8582', 'jasjdkja', '0000-00-00', 9),
(67, 'b0fa0aa447373f9b5230c8e301bc4071', 'asas', '0000-00-00', 9),
(68, 'f7f80276130c52ce0229bd820bc5f76d', 'party', '0000-00-00', 27),
(69, '06b1399d46e79c7551936ed8edf66296', 'BBQ', '0000-00-00', 27),
(70, '6272cab474937b0e39a5e06fdb8c4cee', 'BBQ', '0000-00-00', 34);

-- --------------------------------------------------------

--
-- Table structure for table `pmembers`
--

CREATE TABLE IF NOT EXISTS `pmembers` (
`id` int(8) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `planid` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `pmembers`
--

INSERT INTO `pmembers` (`id`, `userid`, `planid`) VALUES
(1, '0', '5daa21de60354e5b046b52a3398e8582'),
(2, '44933', '5daa21de60354e5b046b52a3398e8582'),
(3, '44933', '5daa21de60354e5b046b52a3398e8582'),
(4, '44933', '5daa21de60354e5b046b52a3398e8582'),
(5, '7981', 'f7f80276130c52ce0229bd820bc5f76d'),
(14, '7ac7e5ec8ce2cf741172e6f1a594d35e', 'f7f80276130c52ce0229bd820bc5f76d'),
(15, '7ac7e5ec8ce2cf741172e6f1a594d35e', 'f7f80276130c52ce0229bd820bc5f76d'),
(16, '7ac7e5ec8ce2cf741172e6f1a594d35e', 'f7f80276130c52ce0229bd820bc5f76d'),
(17, '7ac7e5ec8ce2cf741172e6f1a594d35e', 'f7f80276130c52ce0229bd820bc5f76d'),
(24, '7ac7e5ec8ce2cf741172e6f1a594d35e', '06b1399d46e79c7551936ed8edf66296'),
(25, '7ac7e5ec8ce2cf741172e6f1a594d35e', '06b1399d46e79c7551936ed8edf66296'),
(26, '7ac7e5ec8ce2cf741172e6f1a594d35e', '06b1399d46e79c7551936ed8edf66296'),
(27, '7ac7e5ec8ce2cf741172e6f1a594d35e', '06b1399d46e79c7551936ed8edf66296'),
(28, '815a3b5356089e9988c129d9efe04d85', '06b1399d46e79c7551936ed8edf66296'),
(29, 'e6b04dba57fce23e07d96377724b14db', '06b1399d46e79c7551936ed8edf66296'),
(30, '146391ffc1a19ac64b02afeea843fced', 'f7f80276130c52ce0229bd820bc5f76d'),
(31, '146391ffc1a19ac64b02afeea843fced', '6272cab474937b0e39a5e06fdb8c4cee'),
(32, 'e0ace9eb364012e53b2b024dc1914c56', '6272cab474937b0e39a5e06fdb8c4cee'),
(33, '6af863e2aa70e5a7bcc261edd4ba0c3a', '6272cab474937b0e39a5e06fdb8c4cee');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE IF NOT EXISTS `site_settings` (
`id` int(2) NOT NULL,
  `site_url` varchar(85) NOT NULL,
  `site_email` varchar(85) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `site_url`, `site_email`) VALUES
(1, 'localhost/hackzurich', 'test@localhost.com');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE IF NOT EXISTS `token` (
`id` int(8) NOT NULL,
  `token` varchar(255) NOT NULL,
  `personid` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(8) NOT NULL,
  `username` varchar(8) NOT NULL,
  `password` varchar(32) NOT NULL,
  `temp_pass` varchar(32) NOT NULL,
  `temp_pass_active` int(1) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `email` varchar(35) NOT NULL,
  `dialing_code` int(5) NOT NULL,
  `phone` int(25) NOT NULL,
  `city` varchar(80) NOT NULL,
  `country` varchar(80) NOT NULL,
  `thumb_path` varchar(150) NOT NULL,
  `img_path` varchar(150) NOT NULL,
  `active` int(1) NOT NULL,
  `level_access` int(1) NOT NULL,
  `act_key` varchar(80) NOT NULL,
  `reg_date` varchar(45) NOT NULL,
  `last_active` varchar(50) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `temp_pass`, `temp_pass_active`, `first_name`, `last_name`, `email`, `dialing_code`, `phone`, `city`, `country`, `thumb_path`, `img_path`, `active`, `level_access`, `act_key`, `reg_date`, `last_active`) VALUES
(1, 'user1', '6c90b22e50c986116fff47d48ad9407d', 'd0ygOllq', 0, 'John', 'Doe', 'test@localhost', 254, 723456789, 'Kigali', 'Afghanistan', 'pics/7593uphol1_thumb.jpg', 'pics/7593uphol1.jpg', 1, 2, '', 'Wednesday, Sep 28, 2011, 8:47 am', ''),
(2, 'admin', '317da97d438875c141a4b5f9f67dfdd0', '', 0, 'Site', 'Admin', 'admin@test.com', 254, 722123123, 'Nairobi', 'Kenya', '', '', 1, 1, '', '', ''),
(3, 'user2', '2c5c4e7fd92598deeecbcd8da0edb373', '', 0, 'Jane', 'Doe', 'user2@mail.com', 254, 771771771, 'Meru', 'Kenya', '', '', 1, 2, '', 'Wednesday, Dec 21, 2011, 6:06 am', ''),
(34, 'test', 'eac9f83f474b629b1487210a8204fd63', '', 0, '', '', 'amirs.tafreshi@gmail.com', 0, 0, '', '', '', '', 1, 2, '759136b662e760d709f76ee901c13678', 'Sunday, Oct 12, 2014, 2:25 am', ''),
(35, '', '', '', 0, 'Raul', 'Caterin', 'raul@gmail.com', 0, 0, '', '', '', '', 1, 2, 'e0ace9eb364012e53b2b024dc1914c56', 'Sunday, Oct 12, 2014, 2:27 am', ''),
(36, '', '', '', 0, 'Test2', 'test2', 'test2@test2.com', 0, 0, '', '', '', '', 1, 2, '6af863e2aa70e5a7bcc261edd4ba0c3a', 'Sunday, Oct 12, 2014, 2:35 am', ''),
(33, '', '', '', 0, 'Amir', 'Tafreshi', 'amire.tafreshi@gmail.com', 0, 0, '', '', '', '', 1, 2, '146391ffc1a19ac64b02afeea843fced', 'Sunday, Oct 12, 2014, 2:24 am', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dialing_code`
--
ALTER TABLE `dialing_code`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pmembers`
--
ALTER TABLE `pmembers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=221;
--
-- AUTO_INCREMENT for table `dialing_code`
--
ALTER TABLE `dialing_code`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `pmembers`
--
ALTER TABLE `pmembers`
MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
